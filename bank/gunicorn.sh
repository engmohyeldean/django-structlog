#!/bin/sh

WAIT=2
echo "Waiting $WAIT seconds for databases to be there ..."
sleep $WAIT
python manage.py migrate
python manage.py collectstatic --no-input --clear --verbosity 2
if [ "$DJANGO_BANK_ID" = "westbank" ] ; then
    python manage.py loaddata westbank.json
fi
exec gunicorn --bind 0.0.0.0:8000 --workers 5 bank.wsgi:application --access-logfile - --error-logfile -
