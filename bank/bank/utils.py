def context_processor(request):
    from django.conf import settings

    return {"BANK_ID": settings.BANK_ID, "BANK_LABEL": settings.BANK_ID.title()}
