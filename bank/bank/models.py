import structlog
from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models, transaction
from django.utils import timezone
from django.utils.crypto import get_random_string

logger = structlog.get_logger("structlog")


class InsufficientFunds(ValueError):
    pass


class CustomerManager(BaseUserManager):
    def create(self, name, password):
        """
        Create and save a user with the given name and password.
        """
        customer = self.model(name=name)
        customer.set_password(password)
        with transaction.atomic():
            customer.save(using=self._db)
            Account.objects.create(customer=customer)
        return customer


class Customer(AbstractBaseUser):
    name = models.CharField(max_length=100, unique=True)

    objects = CustomerManager()

    USERNAME_FIELD = "name"

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


def get_account_id():
    def get_id():
        return get_random_string(
            length=10, allowed_chars="ABCDEFGHKLMNPRSTUVWXYZ23456789"
        )

    id = get_id()
    while Account.objects.filter(id=id).exists():
        id = get_id()
    return id


class AccountManager(models.Manager):
    def _deposit(self, account_id, amount):
        with transaction.atomic():
            account = self.select_for_update().get(id=account_id)
            account.balance += amount
            account.save(update_fields=["balance"])

    def deposit(self, account_id, amount):
        assert amount > 0
        logger.info("money_deposit", account=account_id, amount=amount)
        self._deposit(account_id, amount)

    def _transfer(self, from_account_id, to_account_id, amount):
        with transaction.atomic():
            self._withdraw(from_account_id, amount)
            self._deposit(to_account_id, amount)

    def transfer(self, from_account_id, to_account_id, amount):
        assert amount > 0
        logger.info(
            "money_transfer",
            from_account=from_account_id,
            to_account=to_account_id,
            amount=amount,
        )
        self._transfer(from_account_id, to_account_id, amount)

    def _withdraw(self, account_id, amount):
        with transaction.atomic():
            account = self.select_for_update().get(id=account_id)
            if account.balance < amount:
                logger.error(
                    "insufficient_funds",
                    from_account=account_id,
                    available=account.balance,
                )
                raise InsufficientFunds(f"Insufficient funds in account {account_id}")
            account.balance -= amount
            account.save(update_fields=["balance"])

    def withdraw(self, account_id, amount):
        assert amount > 0
        logger.info("money_withdraw", account=account_id, amount=amount)
        self._withdraw(account_id, amount)


class Account(models.Model):
    id = models.CharField(max_length=10, primary_key=True, default=get_account_id)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    customer = models.OneToOneField(Customer, on_delete=models.PROTECT)

    objects = AccountManager()

    class Meta:
        ordering = ("id",)

    def __str__(self):
        return self.id


PP_MAP_INVERSE = {"02": "eastbank", "12": "westbank"}
PP_MAP = {v: [int(n) for n in k] for k, v in PP_MAP_INVERSE.items()}


def get_aba_rtn_check_digit(digits):
    assert len(digits) == 8
    c = (
        3 * (digits[0] + digits[3] + digits[6])
        + 7 * (digits[1] + digits[4] + digits[7])
        + (digits[2] + digits[5])
    ) % 10
    if c > 0:
        c = 10 - c
    return c


def get_aba_rtn():
    pp = PP_MAP[settings.BANK_ID]

    def inner():
        digits = pp + [
            int(d) for d in get_random_string(length=6, allowed_chars="1234567890")
        ]
        # https://en.wikipedia.org/wiki/ABA_routing_transit_number#Check_digit
        c = get_aba_rtn_check_digit(digits)
        digits.append(c)
        return "".join(map(str, digits))

    aba_rtn = inner()
    while Check.objects.filter(aba_rtn=aba_rtn).exists():
        aba_rtn = inner()
    return aba_rtn


class CheckManager(models.Manager):
    def new_checkbook(self, customer: Customer, size=20):
        checks = [Check(customer=customer) for i in range(size)]
        self.bulk_create(checks)


class Check(models.Model):
    aba_rtn = models.CharField(
        editable=False, primary_key=True, max_length=9, default=get_aba_rtn
    )
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    used = models.BooleanField(default=False)

    objects = CheckManager()

    class Meta:
        ordering = ("aba_rtn",)

    def __str__(self):
        return self.aba_rtn

    @property
    def aba_rtn_frrs(self):
        return self.aba_rtn[0:4]

    @property
    def aba_rtn_abaii(self):
        return self.aba_rtn[4:8]

    @property
    def aba_rtn_cd(self):
        return self.aba_rtn[8]


class Notification(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    message = models.TextField()
    timestamp = models.DateTimeField(default=timezone.now, editable=False)

    class Meta:
        ordering = ("-timestamp",)
        indexes = [
            models.Index(
                fields=("customer", "-timestamp"), name="customer_timestamp_desc_idx"
            )
        ]

    @classmethod
    def new(cls, *, account, message, trace_id=None):
        customer = Customer.objects.get(account=account)
        if isinstance(message, list):
            message = "\n\n".join(message)
        message = f"""{message}

—
TRACE: {trace_id}
"""
        cls.objects.create(customer=customer, message=message)
