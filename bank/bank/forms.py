import structlog
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator

from .models import Account, Customer

logger = structlog.get_logger("structlog")


class RegisterForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ("name", "password")
        widgets = {"password": forms.PasswordInput}

    def save(self, *args, **kwargs):
        cd = self.cleaned_data
        customer = Customer.objects.create(cd["name"], cd["password"])
        logger.bind(user_id=customer.id)
        logger.info("new_customer", account=customer.account.id)
        return customer


class DepositWithdrawForm(forms.Form):
    amount = forms.DecimalField(min_value=0, max_digits=10, decimal_places=2)


class TransferForm(forms.Form):
    account_id = forms.ModelChoiceField(queryset=None)
    amount = forms.DecimalField(min_value=0, max_digits=10, decimal_places=2)

    def __init__(self, current_customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_customer = current_customer
        accounts = Account.objects.exclude(customer_id=current_customer.pk).all()
        self.fields["account_id"].queryset = accounts

    def clean_amount(self):
        value = self.cleaned_data["amount"]
        MaxValueValidator(self.current_customer.account.balance)(value)
        return value


class RemoteTransferForm(forms.Form):
    BANK_CHOICE_EASTBANK = "eastbank"
    BANK_CHOICE_WESTBANK = "westbank"
    BANK_CHOICES = (
        (BANK_CHOICE_EASTBANK, "Eastbank"),
        (BANK_CHOICE_WESTBANK, "Westbank"),
    )
    account_id = forms.CharField(max_length=10)
    amount = forms.DecimalField(min_value=0, max_digits=10, decimal_places=2)
    bank = forms.ChoiceField(choices=BANK_CHOICES, initial=settings.BANK_ID)

    def __init__(self, current_customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_customer = current_customer

    def clean_amount(self):
        value = self.cleaned_data["amount"]
        MaxValueValidator(self.current_customer.account.balance)(value)
        return value


def check_aba_rtn(aba_rtn):
    # https://en.wikipedia.org/wiki/ABA_routing_transit_number#Check_digit
    assert len(aba_rtn) == 9
    digits = tuple(int(d) for d in aba_rtn)
    checksum = (
        3 * (digits[0] + digits[3] + digits[6])
        + 7 * (digits[1] + digits[4] + digits[7])
        + (digits[2] + digits[5] + digits[8])
    ) % 10
    if checksum != 0:
        raise ValidationError("Invalid APA RTN")


class CashCheckForm(forms.Form):
    aba_rtn = forms.CharField(label="ABA RTN", max_length=9, validators=[check_aba_rtn])
    amount = forms.DecimalField(min_value=0, max_digits=10, decimal_places=2)
