#!/bin/sh

WAIT=2
echo "Waiting $WAIT seconds for databases to be there ..."
sleep $WAIT
python manage.py migrate
python manage.py collectstatic --no-input --clear --verbosity 2
python manage.py loaddata flights.json
exec gunicorn --bind 0.0.0.0:8000 --workers 5 airline.wsgi:application --access-logfile - --error-logfile -
