# Generated by Django 2.1.7 on 2019-03-24 22:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("airline", "0002_booking_payment_status")]

    operations = [
        migrations.AlterField(
            model_name="booking",
            name="payment_status",
            field=models.CharField(
                choices=[("pending", "Pending"), ("done", "Done")],
                default="pending",
                max_length=7,
            ),
        )
    ]
