from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils.crypto import get_random_string
from django.utils import timezone


class Flight(models.Model):
    departure_airport = models.CharField(max_length=50)
    departure_time = models.TimeField()
    departure_tz = models.CharField(max_length=50)
    arrival_airport = models.CharField(max_length=50)
    arrival_tz = models.CharField(max_length=50)
    duration = models.DurationField()
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return f"{self.departure_airport} - {self.arrival_airport} ({self.price})"


class TravellerManager(BaseUserManager):
    def create(self, username, password, name, dob):
        """
        Create and save a user with the given username, password, name and dob.
        """
        traveller = self.model(username=username, name=name, dob=dob)
        traveller.set_password(password)
        traveller.save(using=self._db)
        return traveller


class Traveller(AbstractBaseUser):
    username = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=250)
    dob = models.DateField()

    objects = TravellerManager()

    USERNAME_FIELD = "username"

    def __str__(self):
        return self.name


def get_booking_id():
    def get_id():
        return get_random_string(
            length=6, allowed_chars="ABCDEFGHKLMNPRSTUVWXYZ23456789"
        )

    id = get_id()
    while Booking.objects.filter(id=id).exists():
        id = get_id()
    return id


class Booking(models.Model):
    PAYMENT_PENDING = "pending"
    PAYMENT_DONE = "done"
    PAYMENT_CHOICES = (
        (PAYMENT_PENDING, PAYMENT_PENDING.title()),
        (PAYMENT_DONE, PAYMENT_DONE.title()),
    )
    id = models.CharField(max_length=6, default=get_booking_id, primary_key=True)
    buyer = models.ForeignKey(
        Traveller, on_delete=models.PROTECT, related_name="purchased_bookings"
    )
    booking_date = models.DateTimeField(default=timezone.now)
    travellers = models.ManyToManyField(Traveller, related_name="bookings")
    flight = models.ForeignKey(
        Flight, on_delete=models.PROTECT, related_name="bookings"
    )
    departure_date = models.DateTimeField()
    arrival_date = models.DateTimeField()
    payment_status = models.CharField(
        max_length=7, choices=PAYMENT_CHOICES, default=PAYMENT_PENDING
    )

    class Meta:
        ordering = ("booking_date",)

    def __str__(self):
        return self.id
